- Provisioned EKS cluster in AWS using terraform.

- Gitlab CI pipelines are configured to deploy the EKS cluster using terraform.

- Repository - https://gitlab.com/Thulasiraman1994/aws-eks-deployments.git  

- Three separate modules were used:	
			1. Networking
			2. Eks-cluster
			3. Worker-nodes (node groups)

- Network policy is not working ideally in AWS EKS as CNI plugins were not installed. Had to manually install Calico CNI. Post that policies works as expected.

- Metric server configured for scrapping resource utilization – For HPA.

- Pod Distribution Budget - Added init containers to nginx deployment, to understand pod recreations as the nginx pod comes up within few seconds its hard to differentiate the new & old pods. 

- New PHP-apache pod created for node affinity rules & Horizontal auto scaling. (just to avoid nginx pod recreation for the Pod Distribution Budget task)

