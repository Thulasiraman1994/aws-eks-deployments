output "cluster_name" {
  value = module.eks_cluster_module.cluster_name
}

output "cluster_endpoint" {
  value = module.eks_cluster_module.cluster_endpoint
}

output "cluster_ca_certificate" {
  value = module.eks_cluster_module.cluster_ca_certificate
}