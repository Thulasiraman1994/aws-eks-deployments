module "network_module" {
    source = "./modules/networking"
    region = var.region
    vpc_cidr = var.vpc_cidr
    project = var.project
    az_count = var.az_count
    subnet_cidr_bits = var.subnet_cidr_bits
}

module "eks_cluster_module" {
    source = "./modules/eks-cluster"
    project = var.project
    vpc_id = module.network_module.vpc_id.id
    public_subnet_ids = module.network_module.public-subnet
    private_subnet_ids = module.network_module.private-subnet
    eks_nodes_sg = module.eks_worker_module.eks_nodes_sg.id
    tags = var.tags
}

module "eks_worker_module" {
    source = "./modules/worker-nodes"
    tags = var.tags
    project = var.project
    vpc_id = module.network_module.vpc_id.id
    private_subnet_ids = module.network_module.private-subnet
    eks_cluster_sg = module.eks_cluster_module.eks_cluster_sg.id
 
}
