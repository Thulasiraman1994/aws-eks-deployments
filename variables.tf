variable region {}
variable aws_access_key {}
variable aws_secret_key {}
variable vpc_cidr {}
variable project {}
variable az_count {}
variable subnet_cidr_bits {}
variable "tags" {
  type        = map(string)
  default = {
    "Project"     = "TerraformEKSWorkshop"
    "Environment" = "Dev"
  }
}
