output "public-subnet" {
    value = aws_subnet.public
}

output "private-subnet" {
    value = aws_subnet.private
}

output "vpc_id" {
    value = aws_vpc.eks-vpc
}