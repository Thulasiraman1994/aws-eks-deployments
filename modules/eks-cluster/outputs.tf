output "eks_cluster_sg" {
    value = aws_security_group.eks_cluster
}

output "cluster_name" {
  value = aws_eks_cluster.eks-cluster.name
}

output "cluster_endpoint" {
  value = aws_eks_cluster.eks-cluster.endpoint
}

output "cluster_ca_certificate" {
  value = aws_eks_cluster.eks-cluster.certificate_authority[0].data
}