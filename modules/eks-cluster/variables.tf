variable project {}
variable public_subnet_ids {}
variable private_subnet_ids {}
variable eks_nodes_sg {}
variable vpc_id {}
variable tags {}