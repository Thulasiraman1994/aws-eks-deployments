# S3 bucket for terroform states
terraform {
  backend "s3" {
    bucket = "eksterraformstates"
    key    = "states/terraform.tfstate"
    region = "us-east-2"
  }
}
